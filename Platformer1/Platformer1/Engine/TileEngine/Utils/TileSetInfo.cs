﻿
namespace Platformer1.Engine.TileEngine.Utils
{
    public struct TileSetInfo
    {
        public string Name;
        public string Path;
        public int TileWidth;
        public int TileHeight;

        public TileSetInfo(string name, string path, int tileWidth, int tileHeight)
        {
            Name = name;
            Path = path;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
        }
    }
}
