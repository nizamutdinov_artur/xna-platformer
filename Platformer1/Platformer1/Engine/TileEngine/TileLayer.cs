﻿namespace Platformer1.Engine.TileEngine
{
   public class TileLayer
   {
       public MapCell[,] LevelData;
       public TileSet TileSet;

       private readonly int _rows;
       private readonly int _colums;

       public TileLayer(int colums, int rows, TileSet tileSet)
       {
           TileSet = tileSet;

           _rows = rows;
           _colums = colums;

           LevelData = new MapCell[colums, rows];

          // Init();

       }

       public int Rows
       {
           get { return _rows; }
       }

       public int Colums
       {
           get { return _colums; }
       }

       private void Init()
       {  
           for (int y = 0; y < _colums; y++)
           {
               for (int x = 0; x < _rows; x++)
               {
                   LevelData[y, x] = new MapCell(1);
               }
           }
       }

      
    }
}
