﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer1.Engine.TileEngine
{
    public class TileMap
    {
        public int MapWidth;
        public int MapHeight;

        public int TileWidth;
        public int TileHeight;

        public List<LayerController> Layers;

        public TileMap(int mapWidth, int mapHeight, int tileWidth, int tileHeight, List<LayerController> layers)
        {
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
            Layers = layers;
        }

        public TileMap(int mapWidth, int mapHeight, int tileWidth, int tileHeight)
        {
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
            Layers = new List<LayerController>();
        }

     
    }

}
