﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer1.Engine.TileEngine
{
   public interface IRectangleShape
   {
       Rectangle Rectangle { get; set; }

       void Draw(SpriteBatch spriteBatch);
   }

}
