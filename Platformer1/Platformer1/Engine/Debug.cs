﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer1.GameObjects;

namespace Platformer1.Engine
{
   public class Debug
   {
       private Texture2D _pixel;

       public Debug(GraphicsDevice graphicsDevice)
       {
           _pixel= new Texture2D(graphicsDevice, 1, 1);
           _pixel.SetData(new[] {Color.Red });
       }

       public void DrawPlatformsWireframe(List<Block> platforms, SpriteBatch spriteBatch)
       {
           foreach (var platform in platforms)
           {
               DrawBorder(_pixel, spriteBatch, platform.Rectangle, 3);
           }

       }


       private void DrawBorder(Texture2D texture,SpriteBatch spriteBatch,Rectangle rectangle, int borderWidth)
       {
         
           // Draw top line
           spriteBatch.Draw(texture, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, borderWidth), Color.White);

           // Draw left line
           spriteBatch.Draw(texture, new Rectangle(rectangle.X, rectangle.Y, borderWidth, rectangle.Height), Color.White);

           // Draw right line
           spriteBatch.Draw(texture, new Rectangle((rectangle.X + rectangle.Width - borderWidth),
                                           rectangle.Y,
                                           borderWidth,
                                           rectangle.Height), Color.White);
           // Draw bottom line
           spriteBatch.Draw(texture, new Rectangle(rectangle.X,
                                           rectangle.Y + rectangle.Height - borderWidth,
                                           rectangle.Width,
                                           borderWidth), Color.White);
       }
    }
}
