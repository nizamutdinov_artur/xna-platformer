﻿using Microsoft.Xna.Framework;

namespace Platformer1.Engine.Module
{


    public static class Collision
    {

        /// <summary>
        /// Checks the side.
        /// </summary>
        /// <param name="r1">The Player.</param>
        /// <param name="r2">The Object.</param>
        /// <returns></returns>
        public static Side CheckSide(Rectangle r1, Rectangle r2)
        {

            if ((r1.Y + r1.Height) > (r2.Y) && (r1.Y + r1.Height) < (r2.Y + 10))
            {
                return Side.Up;
            }

            if (r1.X + r1.Width < r2.X + 10)
            {
                return Side.Left;
            }
            if (r1.X > r2.X + r2.Width - 10)
            {
                return Side.Right;
            }

            //if ((r1.X + r1.Width > r2.X - 10) && (r1.X + 10 < r2.X + r2.Width))
            //{
                if ((r1.Y < r2.Y + r2.Height) && (r1.Y > r2.Y + r2.Height - 10))
                {
                    return Side.Bottom;
                }

            //}

            return Side.Null;
        }


        /// <summary>
        /// Determines whether the specified rectangles is touching.
        /// </summary>
        /// <param name="r1">The first rectangle.</param>
        /// <param name="r2">The second rectangle.</param>
        /// <returns>Collision Result</returns>
        public static bool IsTouching(Rectangle r1, Rectangle r2)
        {
            return (r1.X - r2.Width <= r2.X) && (r1.X + r1.Width >= r2.X) && (r1.Y - r2.Height <= r2.Y) && (r1.Y + r1.Height >= r2.Y);
        }

    }
}
