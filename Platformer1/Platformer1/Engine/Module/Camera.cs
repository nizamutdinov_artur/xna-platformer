﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer1.Engine.Module
{
    public class Camera2D
    {
        #region Fields

        protected Matrix _transform;
        protected Matrix _inverseTransform;
        protected Viewport _viewport;
        protected float _zoom;
        protected int _x;
        protected int _y;

        #endregion

        #region Properties

        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; }
        }

        /// <summary>
        /// Camera View Matrix Property
        /// </summary>
        public Matrix Transform
        {
            get { return _transform; }
            set { _transform = value; }
        }

        /// <summary>
        /// Inverse of the view matrix, can be used to get objects screen coordinates
        /// from its object coordinates
        /// </summary>
        public Matrix InverseTransform
        {
            get { return Matrix.Invert(_transform); }
        }

        public int X
        {
            get { return _x*-1; }
            set { _x = value*-1; }
        }

        public int Y
        {
            get { return _y*-1; }
            set { _y = value*-1; }
        }

        #endregion

        #region Constructor

        public Camera2D(Viewport viewport)
        {
            _zoom = 1.0f;
            _x = 0;
            _y = 0;
            _viewport = viewport;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Update the camera view
        /// </summary>
        public void Update()
        {
            //Clamp zoom value
            _zoom = MathHelper.Clamp(_zoom, 0.0f, 10.0f);
            //Create view matrix
            _transform = Matrix.CreateScale(new Vector3(_zoom, _zoom, 1))*
                         Matrix.CreateTranslation(_x, _y, 1);
        }

        #endregion
    }
}
