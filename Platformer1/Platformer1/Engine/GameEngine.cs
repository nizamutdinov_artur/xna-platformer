﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Platformer1.Engine.Module;
using Platformer1.Engine.TileEngine;
using Platformer1.GameObjects;

namespace Platformer1.Engine
{
    public class GameEngine
    {
        public event EventHandler<TitleArgs> ChangeTitle;

        private readonly GraphicsDeviceManager _graphics;
        private readonly ContentManager _contentManager;
        private readonly CameraController _cameraController;

        private Physics _physics;

        private TileMap _tileMap;

        private Player _player;

        public GameEngine(ContentManager contentManager, GraphicsDeviceManager graphicsDeviceManager, CameraController cameraController)
        {
            _graphics = graphicsDeviceManager;
            _contentManager = contentManager;
            _cameraController = cameraController;

            LoadContent(contentManager, _graphics.GraphicsDevice);
        }

        public void LoadContent(ContentManager contentManager, GraphicsDevice graphicsDevice)
        {
            _tileMap = FileOperation.LoadFromXML(graphicsDevice, Directory.GetCurrentDirectory() + "\\map.xml");

            _player = new Player(graphicsDevice, Color.Black, 50, 96, 50, 0);

            _physics = new Physics();

        }

        public void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            _player.Update(gameTime, keyboardState);

            _physics.Update(_player, _tileMap);

            _cameraController.Update(mouseState , _player.Rectangle);

            //ChangeTitle(this, new TitleArgs(_player.Velocity.X + " " + _player.Velocity.Y));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, _cameraController.TransformMatrix);
                              
            foreach (var layer in _tileMap.Layers)
                layer.Draw(spriteBatch, CalculateDrawRegionSize(), _cameraController.CameraPos / _cameraController.Camera.Zoom);
            
            _player.Draw(spriteBatch);

            spriteBatch.End();
        }

        private int CalculateDrawRegionSize()
        {
            var textureWidth = _tileMap.TileWidth;
            var scale = _cameraController.Camera.Zoom;

            return (int)(textureWidth * 1.5 / scale);
        }
    }

    public class TitleArgs : EventArgs
    {
        public TitleArgs(string title)
        {
            Title = title;
        }

        public string Title { get; set; }   
    }
}
