﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Platformer1.Engine.TileEngine;
using Platformer1.Engine.TileEngine.Utils;

namespace Platformer1.Engine
{
   public class LayerController
   {
       private readonly LayerView view;

       public TileLayer TileMap;

       public string Name;

       public LayerController(GraphicsDevice graphicsDevice, TileSetInfo tileInfo, int mapWidth, int mapHeight)
       {
           Name = tileInfo.Name;

           view = new LayerView(this);

           TileMap = new TileLayer(mapHeight, mapWidth, new TileSet(graphicsDevice, tileInfo.Path, tileInfo.TileWidth, tileInfo.TileHeight));
          
       }


       public LayerController(GraphicsDevice graphicsDevice, TileSetInfo tileInfo, int mapWidth, int mapHeight, MapCell[,] levelData)
       {
           Name = tileInfo.Name;

           view = new LayerView(this);

           TileMap = new TileLayer(mapHeight, mapWidth, new TileSet(graphicsDevice, tileInfo.Path, tileInfo.TileWidth, tileInfo.TileHeight));

           TileMap.LevelData = levelData;

       }

       public void Update(KeyboardState keyboardState, MouseState mouseState)
       {
       }

       public void Draw(SpriteBatch spriteBatch, int size, Vector2 pos)
       {
           view.DrawRegion(spriteBatch, TileMap, size, pos);
       }
   }
}
