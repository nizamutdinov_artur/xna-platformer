﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Platformer1.Engine.Module;

namespace Platformer1.Engine
{
    public class CameraController
    {
        #region fields

        private readonly Camera2D _camera;

        private int prevScrollValue;

        #endregion 

        #region Constructor

        public CameraController(Viewport viewport)
        {
            _camera = new Camera2D(viewport);

            prevScrollValue = 1;
        }

        #endregion

        #region Properties

        public Matrix TransformMatrix
        {
            get { return _camera.Transform; }
        }

        public Camera2D Camera
        {
            get { return _camera; }
        }

        public Vector2 CameraPos
        {
            get { return new Vector2(_camera.X, _camera.Y); }
        }

        #endregion

        #region Methods

        public void Update(MouseState mouseState, Rectangle objRectangle)
        {
            Input(mouseState);

            _camera.Update();

            CameraBinding(objRectangle);
        }

        /// <summary>
        /// Example Input Method, rotates using cursor keys and zooms using mouse wheel
        /// </summary>
        private void Input( MouseState mouseState)
        {
            if (mouseState.ScrollWheelValue > prevScrollValue)
            {
                _camera.Zoom += 0.1f;
                prevScrollValue = mouseState.ScrollWheelValue;
            }
            else if (mouseState.ScrollWheelValue < prevScrollValue)
            {
                _camera.Zoom -= 0.1f;
                prevScrollValue = mouseState.ScrollWheelValue;
            }
        }

        /// <summary>
        /// Camera bind.
        /// </summary>
        /// <param name="obj">The object rectangle.</param>
        private void CameraBinding(Rectangle obj)
        {
            var cameraCenterPosX = _camera.X + Config.ScreenWidth/2;
            var cameraCenterPosY = _camera.Y + Config.ScreenHeight/2;
            var objCenterPosX = (obj.X + obj.Width / 2) * _camera.Zoom;
            var objCenterPosY = (obj.Y + obj.Height / 2) * _camera.Zoom;
            var boundsOffsetX = Config.ScreenWidth / 10; // From center
            var boundsOffsetY = Config.ScreenHeight / 10; //
            var smooth = Config.ScreenWidth/15; // bigger - smoothest

            //Move camera left relative objects
            if (objCenterPosX < cameraCenterPosX - boundsOffsetX)
                _camera.X -= (int)((cameraCenterPosX - objCenterPosX) / smooth);

            if (objCenterPosX > cameraCenterPosX + boundsOffsetX)
                _camera.X += (int)((objCenterPosX - cameraCenterPosX) / smooth); 
            
            if (objCenterPosY < cameraCenterPosY - boundsOffsetY)
                _camera.Y -= (int)((cameraCenterPosY - objCenterPosY) / smooth);

            if (objCenterPosY > cameraCenterPosY + boundsOffsetY)
                _camera.Y += (int)((objCenterPosY - cameraCenterPosY) / smooth);

        

        }

        #endregion
    }
}