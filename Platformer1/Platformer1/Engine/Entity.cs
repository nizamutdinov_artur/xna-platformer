﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Platformer1.Engine
{
    public class Entity
        {

        #region Fields

        internal Texture2D _sprite;
        private Rectangle _rectangle;
        private float _x;
        private float _y;
        private int _width;
        private int _height;
        private float _opacity;

        #endregion

        #region Constructors

        public Entity(ContentManager contentManager, string spriteName, int width, int height, float x, float y)
        {

            Opacity = 1;

            _sprite = contentManager.Load<Texture2D>(spriteName);

            Width = width;
            Height = height;

            X = x;
            Y = y;
        }

        public Entity(GraphicsDevice graphicsDevice, Color color, int width, int height, float x, float y)
        {

            Opacity = 1;

            _sprite = new Texture2D(graphicsDevice, 1, 1);

            _sprite.SetData(new[] {color});

            Width = width;
            Height = height;

            X = x;
            Y = y;
        }

        #endregion
                                                                                         
        #region Properties

        public float X
        {
            get { return _x; }
            set { _x = value; }
        }

        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public float Opacity
        {
            get { return _opacity; }
            set { _opacity = value; }
        }

        public Texture2D Sprite
            {
            get { return _sprite; }
            set { _sprite = value; }
            }

        public Rectangle Rectangle
            {
            get
                {
                _rectangle.X = ( int )_x;
                _rectangle.Y = ( int )_y;
                _rectangle.Width = _width;
                _rectangle.Height = _height;
                return _rectangle;
                }
            set { _rectangle = value; }

            }

        #endregion

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, Rectangle, Color.White*_opacity);
        }

    }
}