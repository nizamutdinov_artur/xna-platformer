﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Platformer1.Engine;

namespace Platformer1.GameObjects
{


    public class Player: Entity
    {
        private const float MoveSpeed = 4f;

        public bool _isJumping;
        private bool _isWalking;

        public Side TouchSide;

        public Vector2 Velocity;


        public Player(ContentManager contentManager, string spriteName, int width, int height, float x, float y) : base(contentManager, spriteName, width, height, x, y)
        {                                                       
            TouchSide = Side.Null;

            _isJumping = false;
        }

        public Player(GraphicsDevice graphicsDevice, Color color, int width, int height, float x, float y): base(graphicsDevice, color, width, height, x, y)
        {
            TouchSide = Side.Null;

            _isJumping = false;
        }

        public void Update(GameTime gameTime, KeyboardState keyboardState )
        {                                     
            Input( keyboardState );

            Movement();

            if (TouchSide != Side.Up)
                Gravity(); 
            

        }

        private void Input(KeyboardState keyboardState)
        {
            _isWalking = false;

            if (keyboardState.IsKeyDown(Keys.D))
            {
                if (TouchSide != Side.Right)
                {
                    _isWalking = true;
                    if (Velocity.X < MoveSpeed)
                    {
                        Velocity.X += Config.AccelerateSpeed;
                    }
                }
            }

            if (keyboardState.IsKeyDown(Keys.A))
            {
                if (TouchSide != Side.Left)
                {
                    _isWalking = true;
                    if (Velocity.X > (MoveSpeed*-1))
                    {
                        Velocity.X -= Config.AccelerateSpeed;
                    }
                }
            }

            if (!_isJumping)
            {
                if (keyboardState.IsKeyDown(Keys.Space))
                {
                    _isJumping = true;
                    Velocity.Y = (Config.Gravity/2f)*-1;
                }
            }

        }

        private void Movement()
        {
            X += Velocity.X;
            Y += Velocity.Y;

            Deacellerate();

        }

        private void Deacellerate()
        {
            if (_isWalking) return;

            if (Velocity.X > 0)
            {
                Velocity.X -= Config.DeaccelerateSpeed;
                if (Velocity.X < 0.1f)
                    Velocity.X = 0;
            }
            else if (Velocity.X < 0)
            {
                Velocity.X += Config.DeaccelerateSpeed;
                if (Velocity.X > 0.1f)
                    Velocity.X = 0;
            }
        }


        private void Gravity()
        {
            if (Velocity.Y < Config.Gravity)
            {
                Velocity.Y += Config.Gravity/100;

                if (!_isJumping)
                    _isJumping = true;
            }
        }
    }
}
