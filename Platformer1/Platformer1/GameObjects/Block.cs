﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Platformer1.Engine;

namespace Platformer1.GameObjects
{
    public class Block : Entity
    {

        public BlockType Type { get; private set; }

        public Block(GraphicsDevice graphicsDevice, Color color, int size, float x, float y, BlockType type) : base(graphicsDevice, color, size, size, x, y)
        {
            Type = type;
        }

        public Block(ContentManager contentManager, string spriteName, int size, float x, float y, BlockType type) : base(contentManager, spriteName, size, size, x, y)
        {
            Type = type;

        }

        public void Color(Color color)
        {
            _sprite.SetData(new[] {color});
        }
    }

    public enum BlockType
    {
        Solid,
        Passable
    }

}