﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Platformer1.Engine;
using Platformer1.GameObjects;

namespace Platformer1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {

        private readonly GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private CameraController _cameraController;

        private GameEngine _gameEngine;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.Title = "Pinkformer";
                                         
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = Config.ScreenHeight;
            graphics.PreferredBackBufferWidth = Config.ScreenWidth;
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            base.Initialize();
            _gameEngine.ChangeTitle += ChangeTitle;
        }

        private void ChangeTitle(object sender, TitleArgs titleArgs)
        {
            Window.Title = titleArgs.Title;
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            _cameraController = new CameraController(GraphicsDevice.Viewport);
            _gameEngine = new GameEngine(Content, graphics, _cameraController);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            _gameEngine.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _gameEngine.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
