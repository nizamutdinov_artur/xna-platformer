﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Platformer1
{
    public static class Config
    {

        static Config()
        {
            Gravity = 10f;
            DeaccelerateSpeed = 0.5f;
            AccelerateSpeed = 1f;
            ScreenHeight = 600;
            ScreenWidth = 800;
        }

        public static float Gravity { get; set; }
        public static float DeaccelerateSpeed { get; set; }
        public static float AccelerateSpeed { get; set; }

        public static int ScreenHeight { get; set; }
        public static int ScreenWidth { get; set; }
    }
}
