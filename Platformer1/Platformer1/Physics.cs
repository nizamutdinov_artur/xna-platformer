﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Platformer1.Engine;
using Platformer1.Engine.Module;
using Platformer1.Engine.TileEngine;
using Platformer1.GameObjects;

namespace Platformer1
{
   public class Physics
    {
       public void Update(Player player , TileMap map)
       {
           PlayerToMapCollision(player, map);
       }

       private void PlayerToMapCollision(Player player, TileMap map)
       {
           int collisionRadius = 5;

           int yStart = (int) MathHelper.Clamp((player.Y/map.TileHeight), 0, map.MapHeight - collisionRadius);
           int xStart = (int) MathHelper.Clamp((player.X/map.TileHeight), 0, map.MapWidth - collisionRadius);

           int yStop = (int) MathHelper.Clamp(yStart+ collisionRadius, 0, map.MapHeight);
           int xStop = (int) MathHelper.Clamp(xStart+ collisionRadius, 0, map.MapWidth);
         
           player.TouchSide = Side.Null;

           for (int i = yStart; i < yStop ; i++)
           {
               for (int j = xStart; j < xStop; j++)
               {
                   foreach (var layer in map.Layers)
                   {
                       if (layer.TileMap.LevelData[i, j].TileIndex != 0)
                       {
                           Rectangle r1 = new Rectangle(j* map.TileWidth, i*map.TileHeight, map.TileWidth, map.TileHeight);
                           Rectangle r2 = new Rectangle((int) player.X, (int) player.Y, player.Width, player.Height);

                           if (r1.Intersects(r2))
                           {
                               ApplyCollision(player, r1, Collision.CheckSide(r2, r1));
                           }

                       }
                   }

               }
           }


       }

       private void ApplyCollision(Player player,Rectangle obj, Side side)
       {
           player.TouchSide = side;

           switch (side)
           {
               case Side.Up:
               {
                   if (player.Velocity.Y > 0)
                   {
                       player.Velocity.Y = 0;
                       player.Y = obj.Y - player.Height + 1;

                       player._isJumping = false;
                   }

                   break;
               }

               case Side.Bottom:
               {
                   if (player.Velocity.Y < 0)
                   {
                       player.Velocity.Y = 0;
                       player.Y = obj.Y + obj.Height;
                   }

                   break;
               }

               case Side.Right:
               {
                   player.Velocity.X = 0;
                   player.X = obj.X + obj.Width;

                   break;
               }

               case Side.Left:
               {
                   player.Velocity.X = 0;
                   player.X = obj.X - player.Width;

                   break;
               }

           }


       }

    }
}
