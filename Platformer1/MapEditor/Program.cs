using System;
using System.Windows.Forms;
using MapEditor.Editor.WinForms;

namespace MapEditor
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //using (Game game = new Game())
            //{
            //    game.Run();
            //}
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            
            using (MapEditorForm mapEditor = new MapEditorForm())
            {
                Application.Run(mapEditor);
            }
        }
    }
#endif
}

