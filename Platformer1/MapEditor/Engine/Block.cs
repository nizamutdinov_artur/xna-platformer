﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor.Engine
{
    public class Block : Entity
    {
        public int ID { get; set; }

        public Block(ContentManager contentManager, string spriteName, int width, int height, float x, float y)
        {

            Alpha = 1;

            _texture = contentManager.Load<Texture2D>(spriteName);

            Width = width;
            Height = height;

            X = x;
            Y = y;

            ID = 0;
        }

        public Block(GraphicsDevice graphicsDevice, Color color, int width, int height, float x, float y)
        {

            Alpha = 1;

            _texture = new Texture2D(graphicsDevice, 1, 1);

            _texture.SetData(new[] {color});

            Width = width;
            Height = height;

            X = x;
            Y = y;

            ID = 0;
        }
    }
}