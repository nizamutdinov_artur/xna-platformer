﻿using MapEditor.Engine.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor.Engine
{
    public class Entity : IRectangleShape
    {
        #region Fields

        protected Texture2D _texture;
        protected Rectangle _rectangle;
        private float _x;
        private float _y;
        private int _width;
        private int _height;
        private float _alpha;

        #endregion

        #region Properties

        public float X
        {
            get { return _x; }
            set { _x = value; }
        }

        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public float Alpha
        {
            get { return _alpha; }
            set { _alpha = value; }
        }

        public Texture2D Texture
        {
            get { return _texture; }
            set { _texture = value; }
        }

        public Rectangle Rectangle
        {
            get
            {
                _rectangle.X = (int) _x;
                _rectangle.Y = (int) _y;
                _rectangle.Width = _width;
                _rectangle.Height = _height;
                return _rectangle;
            }
            set { _rectangle = value; }

        }

        #endregion

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, Rectangle, Color.White*_alpha);
        }

    }
}