﻿using System.Collections.Generic;

namespace MapEditor.Engine.TileEngine
{
    public class Map
    {
        public int MapWidth;
        public int MapHeight;

        public int TileWidth;
        public int TileHeight;

        public List<Controller.LayerController> Layers;

        public Map(int mapWidth, int mapHeight, int tileWidth, int tileHeight, List<Controller.LayerController> layers)
        {
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
            Layers = layers;
        }

        public Map(int mapWidth, int mapHeight, int tileWidth, int tileHeight)
        {
            MapWidth = mapWidth;
            MapHeight = mapHeight;
            TileWidth = tileWidth;
            TileHeight = tileHeight;
            Layers = new List<Controller.LayerController>();
        }
    }

}
