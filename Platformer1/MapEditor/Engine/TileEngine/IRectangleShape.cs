﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor.Engine.TileEngine
{
   public interface IRectangleShape
   {
       Rectangle Rectangle { get; set; }

       void Draw(SpriteBatch spriteBatch);
   }

}
