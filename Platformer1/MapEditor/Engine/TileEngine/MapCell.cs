﻿namespace MapEditor.Engine.TileEngine
{
   public struct MapCell
   {
       public int TileIndex;

       public MapCell(int tileIndex)
       {
           TileIndex = tileIndex;
       }
   }
}
