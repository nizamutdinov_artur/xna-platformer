﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapEditor.Module;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor.Engine.TileEngine
{
   public class TileSet
    {
       public string Path { get; set; }

       private Texture2D _tileSprite;

       private int _textureWidth;
       private int _textureHeight;

       public TileSet(GraphicsDevice graphics, string path, int textureWidth, int textureHeight)
       {
           _tileSprite = TextureLoader.LoadFromFileStream(path, graphics);

           _textureWidth = textureWidth;
           _textureHeight = textureHeight;

           Path = path;
       }

       public int TextureWidth
       {
           get { return _textureWidth; }
           set { _textureWidth = value; }
       }

       public int TextureHeight
       {
           get { return _textureHeight; }
           set { _textureHeight = value; }
       }

       public Texture2D TileSprite
       {
           get { return _tileSprite; }
           set { _tileSprite = value; }
       }

       public Rectangle GetSourceRectangleByID (int textureIndex)
       {
           if (textureIndex != 0)
               textureIndex -= 1;

           int x = textureIndex%(_tileSprite.Width/_textureWidth);
           int y = textureIndex/(_tileSprite.Width/_textureWidth);

           return new Rectangle(x*_textureWidth, y*_textureHeight, _textureWidth, _textureHeight);
       }


    }
}
