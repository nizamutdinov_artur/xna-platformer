﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor.Editor.WinForms
{
    public partial class CreateLayerForm : Form
    {
        public CreateLayerForm()
        {
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
        }



        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK) 
            {
                pathTextBox.Text = openFileDialog1.FileName;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (!System.IO.File.Exists(pathTextBox.Text))
            {
                MessageBox.Show("File isn't exist");
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (nameTextBox.Text.Length < 1)
            {
                MessageBox.Show("Please enter the layer name");
                DialogResult = DialogResult.Cancel;
                return;
            }

            DialogResult = DialogResult.OK;
        }
    }
}
