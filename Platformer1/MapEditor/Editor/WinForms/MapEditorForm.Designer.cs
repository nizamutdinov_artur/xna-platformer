﻿namespace MapEditor.Editor.WinForms
{
    partial class MapEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lListBox = new System.Windows.Forms.ListBox();
            this.newButton = new System.Windows.Forms.Button();
            this.homeButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.lCreateButton = new System.Windows.Forms.Button();
            this.lRemoveButton = new System.Windows.Forms.Button();
            this.xnaDisplay1 = new MapEditor.Editor.WinForms.XNADisplay();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.spriteChooser1 = new MapEditor.Editor.WinForms.SpriteChooser();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lListBox);
            this.panel1.Controls.Add(this.newButton);
            this.panel1.Controls.Add(this.homeButton);
            this.panel1.Controls.Add(this.saveButton);
            this.panel1.Controls.Add(this.loadButton);
            this.panel1.Controls.Add(this.lCreateButton);
            this.panel1.Controls.Add(this.lRemoveButton);
            this.panel1.Controls.Add(this.xnaDisplay1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1084, 661);
            this.panel1.TabIndex = 1;
            // 
            // lListBox
            // 
            this.lListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lListBox.FormattingEnabled = true;
            this.lListBox.Location = new System.Drawing.Point(747, 542);
            this.lListBox.Name = "lListBox";
            this.lListBox.Size = new System.Drawing.Size(323, 82);
            this.lListBox.TabIndex = 3;
            this.lListBox.Visible = false;
            this.lListBox.SelectedIndexChanged += new System.EventHandler(this.lListBox_SelectedIndexChanged);
            this.lListBox.MouseEnter += new System.EventHandler(this.lListBox_MouseEnter);
            // 
            // newButton
            // 
            this.newButton.BackColor = System.Drawing.SystemColors.Control;
            this.newButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newButton.Location = new System.Drawing.Point(10, 10);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(75, 25);
            this.newButton.TabIndex = 1;
            this.newButton.Text = "New";
            this.newButton.UseVisualStyleBackColor = false;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // homeButton
            // 
            this.homeButton.BackColor = System.Drawing.SystemColors.Control;
            this.homeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.homeButton.Location = new System.Drawing.Point(253, 10);
            this.homeButton.Name = "homeButton";
            this.homeButton.Size = new System.Drawing.Size(75, 25);
            this.homeButton.TabIndex = 1;
            this.homeButton.Text = "Home";
            this.homeButton.UseVisualStyleBackColor = false;
            this.homeButton.Visible = false;
            this.homeButton.Click += new System.EventHandler(this.homeButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackColor = System.Drawing.SystemColors.Control;
            this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveButton.Location = new System.Drawing.Point(172, 10);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 25);
            this.saveButton.TabIndex = 1;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = false;
            this.saveButton.Visible = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // loadButton
            // 
            this.loadButton.BackColor = System.Drawing.SystemColors.Control;
            this.loadButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadButton.Location = new System.Drawing.Point(91, 10);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(75, 25);
            this.loadButton.TabIndex = 1;
            this.loadButton.Text = "Load";
            this.loadButton.UseVisualStyleBackColor = false;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // lCreateButton
            // 
            this.lCreateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lCreateButton.BackColor = System.Drawing.SystemColors.Control;
            this.lCreateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lCreateButton.Location = new System.Drawing.Point(914, 627);
            this.lCreateButton.Name = "lCreateButton";
            this.lCreateButton.Size = new System.Drawing.Size(75, 25);
            this.lCreateButton.TabIndex = 1;
            this.lCreateButton.Text = "Create";
            this.lCreateButton.UseVisualStyleBackColor = false;
            this.lCreateButton.Visible = false;
            this.lCreateButton.Click += new System.EventHandler(this.lCreateButton_Click);
            // 
            // lRemoveButton
            // 
            this.lRemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lRemoveButton.BackColor = System.Drawing.SystemColors.Control;
            this.lRemoveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lRemoveButton.Location = new System.Drawing.Point(995, 627);
            this.lRemoveButton.Name = "lRemoveButton";
            this.lRemoveButton.Size = new System.Drawing.Size(75, 25);
            this.lRemoveButton.TabIndex = 1;
            this.lRemoveButton.Text = "Remove";
            this.lRemoveButton.UseVisualStyleBackColor = false;
            this.lRemoveButton.Visible = false;
            this.lRemoveButton.Click += new System.EventHandler(this.lRemoveButton_Click);
            // 
            // xnaDisplay1
            // 
            this.xnaDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xnaDisplay1.Location = new System.Drawing.Point(0, 0);
            this.xnaDisplay1.Name = "xnaDisplay1";
            this.xnaDisplay1.Size = new System.Drawing.Size(1080, 657);
            this.xnaDisplay1.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // spriteChooser1
            // 
            this.spriteChooser1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spriteChooser1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spriteChooser1.Location = new System.Drawing.Point(749, 26);
            this.spriteChooser1.Name = "spriteChooser1";
            this.spriteChooser1.Size = new System.Drawing.Size(323, 291);
            this.spriteChooser1.TabIndex = 2;
            this.spriteChooser1.Visible = false;
            // 
            // MapEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(1084, 661);
            this.Controls.Add(this.spriteChooser1);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.Name = "MapEditorForm";
            this.ShowIcon = false;
            this.Text = "MapEditor";
            this.Load += new System.EventHandler(this.MapEditorForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MapEditor_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.MapEditor_KeyUp);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private XNADisplay xnaDisplay1;
        private System.Windows.Forms.Panel panel1;
        private SpriteChooser spriteChooser1;
        private System.Windows.Forms.ListBox lListBox;
        private System.Windows.Forms.Button lCreateButton;
        private System.Windows.Forms.Button lRemoveButton;
        private System.Windows.Forms.Button newButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button homeButton;
    }
}