﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MapEditor.Editor.WinForms
{
    public partial class SpriteChooser : UserControl
    {
        public SpriteChooser()
        {
            InitializeComponent();
        }

        public int GetTileID(Point mousePos, int tileWidth, int tileHeight, int textureWidth)
        {
            int imageWidth = textureWidth;

            int texturesInColumn = imageWidth / tileWidth;

            int x = mousePos.X / (tileWidth) + 1;
            int y = mousePos.Y / (tileHeight);

            int id = y * texturesInColumn + x;
            return id;
        }


        public string ImageLocation
        {
            set { pictureBox1.ImageLocation = value; }
        }

    }
}
