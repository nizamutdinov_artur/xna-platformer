﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using MapEditor.Engine;
using MapEditor.Module;
using MapEditor.Utils;
using Keyboard = MapEditor.Module.Input.Keyboard;
using KeyEventArgs = System.Windows.Forms.KeyEventArgs;

namespace MapEditor.Editor.WinForms
{
    public partial class MapEditorForm : System.Windows.Forms.Form
    {

        public MapEditorForm()
        {
            InitializeComponent();

            xnaDisplay1.MouseWheel += Input.Mouse.MouseWheel;
            xnaDisplay1.MouseDown += Input.Mouse.MouseDown;
            xnaDisplay1.MouseUp += Input.Mouse.MouseUp;

            xnaDisplay1.MouseMove += (o, mouseEventArgs) => { xnaDisplay1.Focus(); };

            spriteChooser1.pictureBox1.Click += (pictureBox1_Click);


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            EditorEngine editorEngine = xnaDisplay1.EditorEngine;

            if (editorEngine.Map.Layers.Count == 0)
                return;

            SetImage();

            Point mPos = spriteChooser1.pictureBox1.PointToClient(MousePosition);

            int textureWidth = editorEngine.Map.Layers[editorEngine.CurentLayerID].TileMap.TileSet.TileSprite.Width;

            int tileWidth = editorEngine.Map.Layers[editorEngine.CurentLayerID].TileMap.TileSet.TextureWidth;
            int tileHeight = editorEngine.Map.Layers[editorEngine.CurentLayerID].TileMap.TileSet.TextureHeight;

            Cursor cursor = editorEngine.Map.Layers[editorEngine.CurentLayerID].Cursor;
            cursor.Brush.ID = spriteChooser1.GetTileID(mPos, tileWidth, tileHeight, textureWidth);

            // TODO: REFRACTOR

        }

        private void UpdateListBox()
        {
            var layers = xnaDisplay1.EditorEngine.Map.Layers;

            lListBox.Items.Clear();

            for (int i = 0; i < layers.Count; i++)
            {
                lListBox.Items.Add(layers[i].Name);
            }

            int index = xnaDisplay1.EditorEngine.CurentLayerID;

            if (lListBox.Items.Count > index)
            {
                lListBox.SelectedIndex = index;
            }

        }

        private void MapEditor_KeyUp(object sender, KeyEventArgs e)
        {
            Keyboard.SetKeyUp(e.KeyCode);
        }

        private void MapEditor_KeyDown(object sender, KeyEventArgs e)
        {
            Keyboard.SetKeyDown(e.KeyCode);

        }

        private void MapEditorForm_Load(object sender, EventArgs e)
        {
            UpdateListBox();
        }

        private void lCreateButton_Click(object sender, EventArgs e)
        {
            CreateLayerForm createLayerForm = new CreateLayerForm();

            if (createLayerForm.ShowDialog() == DialogResult.OK)
            {
                CreateLayer(createLayerForm.nameTextBox.Text, createLayerForm.pathTextBox.Text);

                SetImage();
            }
        }

        private void SetImage()
        {
            if (xnaDisplay1.EditorEngine.Map.Layers.Count > xnaDisplay1.EditorEngine.CurentLayerID)
            {
                spriteChooser1.pictureBox1.ImageLocation = xnaDisplay1.EditorEngine.Map.Layers[xnaDisplay1.EditorEngine.CurentLayerID].TileMap.TileSet.Path;
            }
        }

        private void CreateLayer(string name, string tileSetPath)
        {
            xnaDisplay1.EditorEngine.CreateLayer(name, tileSetPath);
        }

        private void lListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lListBox.SelectedIndex >= 0)
            {
                xnaDisplay1.EditorEngine.CurentLayerID = lListBox.SelectedIndex;
                SetImage();
            }
        }

        private void lListBox_MouseEnter(object sender, EventArgs e)
        {
            UpdateListBox();
        }

        private void lRemoveButton_Click(object sender, EventArgs e)
        {
            if (lListBox.SelectedIndex >= 0)
                xnaDisplay1.EditorEngine.RemoveLayer(lListBox.SelectedIndex);
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            NewMapForm newMapForm = new NewMapForm();

            if (newMapForm.ShowDialog() == DialogResult.OK)
            {
                xnaDisplay1.NewMap(
                    (int) newMapForm.mapHeightNum.Value,
                    (int) newMapForm.mapWidthNum.Value,
                    (int) newMapForm.tileHeightNum.Value,
                    (int) newMapForm.tileWidthNum.Value);

                saveButton.Visible = true;
                spriteChooser1.Visible = true;
                lListBox.Visible = true;
                lCreateButton.Visible = true;
                lRemoveButton.Visible = true;
                homeButton.Visible = true;
            }
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                xnaDisplay1.EditorEngine.Load(openFileDialog1.FileName);

                saveButton.Visible = true;
                spriteChooser1.Visible = true;
                lListBox.Visible = true;
                lCreateButton.Visible = true;
                lRemoveButton.Visible = true;
                homeButton.Visible = true;
            }



            UpdateListBox();

        }


        private void saveButton_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                xnaDisplay1.EditorEngine.Save(saveFileDialog1.FileName);

            UpdateListBox();
        }

        private void homeButton_Click(object sender, EventArgs e)
        {

            xnaDisplay1.EditorEngine.CameraController.Camera.X = 0;
            xnaDisplay1.EditorEngine.CameraController.Camera.Y = 0;
            xnaDisplay1.EditorEngine.CameraController.Camera.Zoom = 1;
        }

    }
}