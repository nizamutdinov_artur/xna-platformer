﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor.Editor.WinForms
{
    public partial class NewMapForm : Form
    {
        public NewMapForm()
        {
            InitializeComponent();
            mapWidthNum.ValueChanged += ValueChanged;
            mapHeightNum.ValueChanged += ValueChanged;
            tileWidthNum.ValueChanged += ValueChanged;
            tileHeightNum.ValueChanged += ValueChanged;
        }

        void ValueChanged(object sender, EventArgs e)
        {
            CalcResolution();
        }

        private void NewMapForm_Load(object sender, EventArgs e)
        {
         CalcResolution();
        }

        private void CalcResolution()
        {
            var h = (mapHeightNum.Value*tileHeightNum.Value);
            var w = (mapWidthNum.Value*tileWidthNum.Value);

            sizeLabel.Text = w + "x" + h + "px";
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
