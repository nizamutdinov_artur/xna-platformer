﻿using System.Diagnostics;
using System.Windows.Forms;
using MapEditor.Engine;
using MapEditor.Engine.WinFormGraphicsDevice;
using MapEditor.Module;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor.Editor.WinForms
{
    public partial class XNADisplay : GraphicsDeviceControl
    {
        private Stopwatch timer;
        private ContentManager content;
        private SpriteBatch spriteBatch;                       

        private EditorEngine _editorEngine;

       

        public XNADisplay()
        {
            InitializeComponent();
        }

        public EditorEngine EditorEngine
        {
            get { return _editorEngine; }
        }

        protected override void Initialize()
        {
            ResizeRedraw = false;
            content = new ContentManager(Services, "Content");
            spriteBatch = new SpriteBatch(GraphicsDevice);

            _editorEngine = new EditorEngine(spriteBatch, content, GraphicsDevice, this,50,50,32,32);

            // Start the animation timer.
            timer = Stopwatch.StartNew();

            // Hook the idle event to constantly redraw our animation.
            Application.Idle += delegate { Invalidate(); };
        }

        protected override void Draw()
        {
            GraphicsDevice.Clear(Color.Green);

            Input.Mouse.UpdatePosition(PointToClient(MousePosition), EditorEngine.CameraController.Camera);

            _editorEngine.Update(null);
            _editorEngine.Draw();
        }


        public void NewMap(int mapHeight, int mapWidth, int tileHeight, int tileWidth)
        {                                                 
            _editorEngine = new EditorEngine(spriteBatch,content,GraphicsDevice,this, mapWidth,mapHeight,tileWidth,tileHeight);       
        }
    }
}
