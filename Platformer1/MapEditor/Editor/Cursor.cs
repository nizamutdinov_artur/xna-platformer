﻿using MapEditor.Engine.TileEngine;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapEditor.Editor
{
   public class Cursor
   {
       private TileLayer _tileMap;

       private BrushBlock _brush;

       private bool erase = false;
       private int prevID;

       public Cursor(GraphicsDevice graphicsDevice, TileLayer tileMap)
       {
           _tileMap = tileMap;

           _brush = new BrushBlock(graphicsDevice, tileMap);
       }

       public TileLayer TileMap
       {
           get { return _tileMap; }
           set { _tileMap = value; }
       }

       public BrushBlock Brush
       {
           get { return _brush; }
           set { _brush = value; }
       }

       public void Update(KeyboardState keyboardState, MouseState mouseState)
       {
           _brush.Update(keyboardState, mouseState);
           Input(keyboardState, mouseState);

       }

       private void Input(KeyboardState keyboardState, MouseState mouseState)
       {
           _brush.X = mouseState.X - (mouseState.X%_tileMap.TileSet.TextureWidth);
           _brush.Y = mouseState.Y - (mouseState.Y%_tileMap.TileSet.TextureHeight);

           if (mouseState.LeftButton == ButtonState.Pressed)
           {
               if (erase)
               {
                   _brush.ID = prevID;
                   erase = false;
               }
               _brush.Paint();
           }

           if (mouseState.RightButton == ButtonState.Pressed)
           {
               if (!erase)
               {
                   erase = true;
                   prevID =  _brush.ID;
                   _brush.ID = 0;
               }
               _brush.Erase();

           }
       }

       public void Draw(SpriteBatch spriteBatch)
       {
           _brush.Draw(spriteBatch);
       }
   }
}
