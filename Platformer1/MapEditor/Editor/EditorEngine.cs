﻿using System.Collections.Generic;
using System.IO;
using MapEditor.Controller;
using MapEditor.Editor.WinForms;
using MapEditor.Engine.TileEngine;
using MapEditor.Module;
using MapEditor.Utils;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapEditor.Editor
{
    public class EditorEngine
    {
        public XNADisplay XnaControl;

        private readonly SpriteBatch spriteBatch;
        private readonly GraphicsDevice graphics;
        private ContentManager content;

        public CameraController CameraController;

        public Map Map;

        public int CurentLayerID;

        public EditorEngine(SpriteBatch spriteBatch, ContentManager content, GraphicsDevice graphics, XNADisplay xnaDisplay, int mapWidth, int mapHeight, int tileWidth, int tileHeight)
        {
            CurentLayerID = 0;
            XnaControl = xnaDisplay;

            this.content = content;
            this.spriteBatch = spriteBatch;
            this.graphics = graphics;

            Map = new Map(mapWidth, mapHeight, tileWidth, tileHeight);

            Initialize();
        }


        public void CreateLayer(string name, string tileSetPath)
        {
            Map.Layers.Add(new LayerController(graphics, new TileSetInfo(name, tileSetPath, Map.TileWidth, Map.TileHeight), Map.MapWidth, Map.MapHeight));
        }

        public void RemoveLayer(int id)
        {
            if (Map.Layers.Count > id)
                Map.Layers.RemoveAt(id);
        }

        private void Initialize()
        {
            CameraController = new CameraController(graphics.Viewport);
            Map.Layers = new List<LayerController>();
        }


        public void Update(GameTime gameTime)
        {
            MouseState mouseState = Input.Mouse.GetState();
            KeyboardState keyboardState = Input.Keyboard.GetState();

            CameraController.Update(keyboardState, mouseState);

            if (Map.Layers.Count > CurentLayerID)
                Map.Layers[CurentLayerID].Update(keyboardState, mouseState);
        }

        public void Load(string path)
        {
            Map = FileOperation.LoadFromXML(graphics, path);
        }

        public void Save(string path)
        {
            FileOperation.SaveToXML(Map, path);
        }

        private int CalculateDrawRegionSize()
        {
            var textureWidth = Map.TileWidth;
            var scale = CameraController.Camera.Zoom;

            return (int)(textureWidth*1.5/scale);
        }

        public void Draw()
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, null, CameraController.TransformMatrix);

            foreach (var layer in Map.Layers)
                layer.Draw(spriteBatch,CalculateDrawRegionSize(),CameraController.CameraPos/CameraController.Camera.Zoom);

            if (Map.Layers.Count > CurentLayerID)
                Map.Layers[CurentLayerID].Cursor.Draw(spriteBatch);

            spriteBatch.End();
        }


    }
}
