﻿using System;
using MapEditor.Engine;
using MapEditor.Engine.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapEditor.Editor
{
    public class BrushBlock : Block, ICloneable
    {
        private readonly Texture2D _defaultTexture;
        private readonly TileLayer _tileMap;

        public BrushBlock(GraphicsDevice graphicsDevice, TileLayer tileMap) : base(graphicsDevice, Color.CornflowerBlue, tileMap.TileSet.TextureWidth, tileMap.TileSet.TextureHeight, 0, 0)
        {
            Alpha = 1f;

            _tileMap = tileMap;
            _defaultTexture = _texture;
            _texture = _tileMap.TileSet.TileSprite;

        }

        public void Update(KeyboardState keyboardState, MouseState mouseState)
        {
        }

        public void Paint()
        {
            int x = (int) (X/_tileMap.TileSet.TextureWidth);
            int y = (int) (Y/_tileMap.TileSet.TextureHeight);

            if (x < _tileMap.Rows && y < _tileMap.Colums && x >= 0 && y >= 0)
            {
                _tileMap.LevelData[y, x].TileIndex = ID;
            }

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (ID == 0)
            {
                spriteBatch.Draw(_defaultTexture, Rectangle, Color.White*0.5f);
                return;
            }

            spriteBatch.Draw(_texture, Rectangle, _tileMap.TileSet.GetSourceRectangleByID(ID), Color.White*Alpha);
        }

        internal void Erase()
        {
            int x = (int) (X/_tileMap.TileSet.TextureWidth);
            int y = (int) (Y/_tileMap.TileSet.TextureHeight);

            if (x < _tileMap.Rows && y < _tileMap.Colums && x >= 0 && y >= 0)
            {
                _tileMap.LevelData[y, x].TileIndex = 0;
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}