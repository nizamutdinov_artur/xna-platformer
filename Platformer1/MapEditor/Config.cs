﻿using System.Collections.Generic;
using System.IO;
using MapEditor.Engine.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor
{
    public static class Config
    {

        static Config()
        {
            Gravity = 10f;
            DeaccelerateSpeed = 0.5f;
            AccelerateSpeed = 1f;
            ScreenHeight = 600;
            ScreenWidth = 600;

            PlayerSize = new Vector2(50, 100);
        }


        public static float Gravity { get; set; }
        public static float DeaccelerateSpeed { get; set; }
        public static float AccelerateSpeed { get; set; }

        public static int ScreenHeight { get; set; }
        public static int ScreenWidth { get; set; }

        public static Vector2 PlayerSize { get; set; }

    }
}
