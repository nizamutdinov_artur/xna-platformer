﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MapEditor.Controller;
using MapEditor.Engine.TileEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MapEditor.View
{
    public class LayerView
    {
        private LayerController controller;

        public LayerView(LayerController controller)
        {
            this.controller = controller;
        }

        public void DrawMap(SpriteBatch spriteBatch, TileLayer tileMap)
        {

            int textureWidth = tileMap.TileSet.TextureWidth;
            int textureHeight = tileMap.TileSet.TextureHeight;

            int rows = tileMap.Rows;
            int colums = tileMap.Colums;

            for (int y = 0; y < colums; y++)
            {
                for (int x = 0; x < rows; x++)
                {
                    if (tileMap.LevelData[y, x].TileIndex != 0)
                    {
                        spriteBatch.Draw(tileMap.TileSet.TileSprite,
                            new Rectangle(
                                x*textureWidth,
                                y*textureHeight,
                                textureWidth, textureHeight),
                            tileMap.TileSet.GetSourceRectangleByID(tileMap.LevelData[y, x].TileIndex),
                            Color.White);
                    }
                }
            }
        }

        public void DrawRegion(SpriteBatch spriteBatch, TileLayer tileMap, int size, Vector2 pos)
        {

            int textureWidth = tileMap.TileSet.TextureWidth;
            int textureHeight = tileMap.TileSet.TextureHeight;

            int clampY = (int)MathHelper.Clamp( (int)(pos.Y/textureWidth), 0, tileMap.Colums);
            int clampX = (int)MathHelper.Clamp( (int)(pos.X/textureWidth), 0, tileMap.Rows);

            int clampSizeY = (int)MathHelper.Clamp(clampY + size, 0, tileMap.Colums);
            int clampSizeX = (int)MathHelper.Clamp(clampX + size, 0, tileMap.Rows);

            for (int y = clampY; y < clampSizeY; y++)
            {
                for (int x = clampX; x < clampSizeX; x++)
                {
                    if (tileMap.LevelData[y, x].TileIndex != 0)
                    {
                        spriteBatch.Draw(tileMap.TileSet.TileSprite,
                            new Rectangle(
                                x*textureWidth,
                                y*textureHeight,
                                textureWidth, textureHeight),
                            tileMap.TileSet.GetSourceRectangleByID(tileMap.LevelData[y, x].TileIndex),
                            Color.White);
                    }
                }
            }

        }



    }
}
