﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace MapEditor.Module
{
    public static class Input
    {
        public static class Keyboard
        {
            private static Dictionary<Keys, bool> keysDictionary;


            /// <summary>
            /// Initializes the <see cref="Keyboard"/> class.
            /// This class provides a basic functionality of
            /// xna Keyboard class on WinForms.
            /// See <see cref="SetKeyDown"/> , <see cref="SetKeyUp"/> functions.
            /// </summary>
            static Keyboard()
            {
                keysDictionary = new Dictionary<Keys, bool>();

                foreach (Keys key in Enum.GetValues(typeof (Keys)))
                    keysDictionary.Add(key, false);
            }

            public static KeyboardState GetState()
            {
                List<Keys> pressedKeys = new List<Keys>();

                foreach (KeyValuePair<Keys, bool> key in keysDictionary)
                {
                    if (key.Value)
                    {
                        pressedKeys.Add(key.Key);
                    }
                }
                return new KeyboardState(pressedKeys.ToArray());
            }

            public static void SetKeyDown(System.Windows.Forms.Keys key)
            {
                try
                {
                    keysDictionary[(Keys) key] = true;
                }
                catch (Exception)
                {
                    throw new Exception("Wrong key to SetKeyDown");
                }
            }

            public static void SetKeyUp(System.Windows.Forms.Keys key)
            {
                try
                {
                    keysDictionary[(Keys) key] = false;
                }
                catch (Exception)
                {
                    throw new Exception("Wrong key to SetKeyUp");
                }

            }
        }

        public static class Mouse
        {

            private static int scrollWheel;
            public static Vector2 mousePosition;
            private static ButtonState leftButton;
            private static ButtonState middleButton;
            private static ButtonState rightButton;
            private static ButtonState xButton1;
            private static ButtonState xButton2;

            static Mouse()
            {
                scrollWheel = 0;

                leftButton = ButtonState.Released;
                middleButton = ButtonState.Released;
                rightButton = ButtonState.Released;
                xButton1 = ButtonState.Released;
                xButton2 = ButtonState.Released;
            }

            public static MouseState GetState()
            {

                return new MouseState((int)mousePosition.X, (int)mousePosition.Y, scrollWheel, leftButton, middleButton, rightButton, xButton1, xButton2);
            }


            public static void MouseWheel(object sender, MouseEventArgs e)
            {
                scrollWheel += e.Delta;
            }

            public static void MouseDown(object sender, MouseEventArgs e)
            {
                switch (e.Button)
                {
                    case MouseButtons.Left:
                        leftButton = ButtonState.Pressed;
                        break;
                    case MouseButtons.Middle:
                        middleButton = ButtonState.Pressed;
                        break;
                    case MouseButtons.Right:
                        rightButton = ButtonState.Pressed;
                        break;
                    case MouseButtons.XButton1:
                        xButton1 = ButtonState.Pressed;
                        break;
                    case MouseButtons.XButton2:
                        xButton2 = ButtonState.Pressed;
                        break;
                }
            }

            public static void MouseUp(object sender, MouseEventArgs e)
            {
                switch (e.Button)
                {
                    case MouseButtons.Left:
                        leftButton = ButtonState.Released;
                        break;
                    case MouseButtons.Middle:
                        middleButton = ButtonState.Released;
                        break;
                    case MouseButtons.Right:
                        rightButton = ButtonState.Released;
                        break;
                    case MouseButtons.XButton1:
                        xButton1 = ButtonState.Released;
                        break;
                    case MouseButtons.XButton2:
                        xButton2 = ButtonState.Released;
                        break;
                }
            }

            public static void UpdatePosition(System.Drawing.Point mousePos, Camera2D camera)
            {
                mousePosition = GameWorldPos(camera, mousePos.X, mousePos.Y);
            }


            private static Vector2 GameWorldPos(Camera2D camera, int mouseX, int mouseY)
            {
                return new Vector2(
                    ((camera.X * -1) / camera.Zoom + mouseX / camera.Zoom),
                    ((camera.Y * -1) / camera.Zoom + mouseY / camera.Zoom)
                    );
            }



        }
    }

}

