﻿using System.IO;
using MapEditor.Editor;
using MapEditor.Editor.WinForms;
using MapEditor.Engine.TileEngine;
using MapEditor.Utils;
using MapEditor.View;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapEditor.Controller
{
   public class LayerController
   {
       private readonly LayerView view;

       public TileLayer TileMap;
       public Cursor Cursor;

       public string Name;

       public LayerController(GraphicsDevice graphicsDevice, TileSetInfo tileInfo, int mapWidth, int mapHeight)
       {
           Name = tileInfo.Name;

           view = new LayerView(this);

           TileMap = new TileLayer(mapHeight, mapWidth, new TileSet(graphicsDevice, tileInfo.Path, tileInfo.TileWidth, tileInfo.TileHeight));

           Cursor = new Cursor(graphicsDevice, TileMap);
          
       }


       public LayerController(GraphicsDevice graphicsDevice, TileSetInfo tileInfo, int mapWidth, int mapHeight, MapCell[,] levelData)
       {
           Name = tileInfo.Name;

           view = new LayerView(this);

           TileMap = new TileLayer(mapHeight, mapWidth, new TileSet(graphicsDevice, tileInfo.Path, tileInfo.TileWidth, tileInfo.TileHeight));

           Cursor = new Cursor(graphicsDevice, TileMap);

           TileMap.LevelData = levelData;

       }


       public void Update(KeyboardState keyboardState, MouseState mouseState)
       {
           Cursor.Update(keyboardState, mouseState);
       }

       public void Draw(SpriteBatch spriteBatch, int size, Vector2 pos)
       {
           view.DrawRegion(spriteBatch, TileMap, size, pos);
       }
   }
}
