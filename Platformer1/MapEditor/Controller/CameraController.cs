﻿using MapEditor.Module;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MapEditor.Controller
{
    public class CameraController
    {
        private readonly Camera2D _camera;

        private int prevScrollValue;

        public CameraController(Viewport viewport)
        {
            _camera = new Camera2D(viewport);

            prevScrollValue = 1;
        }

        public Matrix TransformMatrix
        {
            get { return _camera.Transform; }
        }

        public Camera2D Camera
        {
            get { return _camera; }
        }

        public Vector2 CameraPos
        {
            get { return new Vector2(_camera.X*-1, _camera.Y*-1); }
        }

        public void Update(KeyboardState keyboardState, MouseState mouseState)
        {
            Input(keyboardState, mouseState);

            _camera.Update();
        }


        /// <summary>
        /// Example Input Method, rotates using cursor keys and zooms using mouse wheel
        /// </summary>
        private void Input(KeyboardState keyboardState, MouseState mouseState)
        {
            if (mouseState.ScrollWheelValue > prevScrollValue)
            {
                _camera.Zoom += 0.1f;
                prevScrollValue = mouseState.ScrollWheelValue;
            }
            else if (mouseState.ScrollWheelValue < prevScrollValue)
            {
                _camera.Zoom -= 0.1f;
                prevScrollValue = mouseState.ScrollWheelValue;
            }

            // Middle button handle
            if (mouseState.MiddleButton == ButtonState.Pressed)
                CameraBinding(new Vector2(mouseState.X, mouseState.Y));

            KeyBoardControl(keyboardState);
        }

        private void KeyBoardControl(KeyboardState keyboardState)
        {
            const int moveSpeed = 3;

            if (keyboardState.IsKeyDown(Keys.A))
                _camera.X += moveSpeed;

            if (keyboardState.IsKeyDown(Keys.D))
                _camera.X -= moveSpeed;

            if (keyboardState.IsKeyDown(Keys.S))
                _camera.Y -= moveSpeed;

            if (keyboardState.IsKeyDown(Keys.W))
                _camera.Y += moveSpeed;
        }

        /// <summary>
        /// Cameras the binding.
        /// </summary>
        /// <param name="objPos">The object position.</param>
        private void CameraBinding(Vector2 objPos)
        {
            var cameraCenterPosX = (_camera.X*-1) + Config.ScreenWidth/2;
            var cameraCenterPosY = (_camera.Y*-1) + Config.ScreenHeight/2;
            var boundsOffsetX = Config.ScreenWidth/10;
            var boundsOffsetY = Config.ScreenHeight/10;
            var smooth = Config.ScreenWidth/10; // bigger - smoothest
            const int objectWidth = 0;

            //Move camera left relative objects
            if (objPos.X*_camera.Zoom + boundsOffsetX < cameraCenterPosX)
                _camera.X += (int) ((cameraCenterPosX - (objPos.X*_camera.Zoom + objectWidth))/smooth);

            if ((objPos.X + Config.PlayerSize.X/2)*_camera.Zoom - boundsOffsetX > cameraCenterPosX)
                _camera.X -= (int) (((objPos.X*_camera.Zoom - objectWidth) - cameraCenterPosX)/smooth);

            if (objPos.Y*_camera.Zoom + boundsOffsetX < cameraCenterPosY)
                _camera.Y += (int) ((cameraCenterPosY - (objPos.Y*_camera.Zoom + objectWidth))/smooth);

            if ((objPos.Y + Config.PlayerSize.Y/2)*_camera.Zoom - boundsOffsetY > cameraCenterPosY)
                _camera.Y -= (int) (((objPos.Y*_camera.Zoom - objectWidth) - cameraCenterPosY)/smooth);

        }
    }
}